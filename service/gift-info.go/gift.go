package main

import (
	"github.com/gin-gonic/gin"
)

var router *gin.Engine

func main() {
	handleRequest()
}

func handleRequest() {
	router = gin.Default()
	router.GET("/", func(c *gin.Context) {

	})
	router.Run()
}