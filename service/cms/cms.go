package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	handleRequest()
}

func cms(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "hello")
}

func handleRequest() {
	fmt.Println("Hello")
	http.HandleFunc("/cms", cms)
	log.Fatal(http.ListenAndServe(":10001", nil))
}
