# Pre
### Docker:
- Create network:
    - docker network create tutt
    - docker network create tutt-brigde --driver bridge
- Connect network
    - docker network connect tutt virtual-gift
### [MySQL](https://hub.docker.com/_/mysql):
- Start MySQL:
    - docker run --name virtual-gift -e MYSQL_ROOT_PASSWORD=123456 -d mysql:8
- Shell access:
    - docker exec -it virtual-gift bash
    - docker logs virtial-gift
- Connect:
    - docker run -it --network tutt --rm mysql:8 mysql -hvirtual-gift -uroot -p123456
- Create databse dump:
    - docker exec virtual-gift sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /all-databases.sql
### [Redis](https://hub.docker.com/_/redis):
- Start Redis:
    - docker run --name virtual-gift-redis -d redis:6
- Connect via cli:
    - docker run -it --network tutt --rm redis:6 redis-cli -h virtual-gift-redis
### [Kafka](https://hub.docker.com/r/bitnami/kafka):
- Start Kafka:
    - docker run --name kafka -d bitnami/kafka:2
    - docker run -d --name kafka-server --network tutt -e ALLOW_PLAINTEXT_LISTENER=yes -e KAFKA_CFG_ZOOKEEPER_CONNECT=zookeeper-server:2181 bitnami/kafka:2
- Connect:
    - docker run -it --network tutt --rm bitnami/kafka:2 kafka-topics.sh --list
#### Zookeeper:
- Start:
    - docker run -d --name zookeeper-server --network tutt -e ALLOW_ANONYMOUS_LOGIN=yes bitnami/zookeeper:latest

### Golang:
- go get github.com/go-sql-driver/mysql

# Tasks:
- Database:
    - [ ] Design: User, Gift
    - [ ] 
- Cache
- Service
- Note:
    - docker run -it --network tutt 4211320b4b50 bash