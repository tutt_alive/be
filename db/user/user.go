package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	fmt.Println("gift")

	db, err := sql.Open("mysql", "root:123456@tcp(virtual-gift:3306)/live")

	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	users, err := db.Query("SELECT * FROM users;")
	if err != nil {
		panic(err.Error())
	}

	for users.Next() {
		var user User
		err = users.Scan(&user.ID, &user.Name, &user.Diamond, &user.Avatar)
		if err != nil {
			panic(err.Error())
		}

		fmt.Println(user.Name)
	}

	defer users.Close()
}

//User ...
type User struct {
	ID      int
	Name    string
	Diamond int64
	Avatar  string
}
