package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

//User ...
type User struct {
	ID      int
	Name    string
	Diamond int64
	Avatar  string
}

// Gift ...
type Gift struct {
	ID              string
	Name            string
	Image           string
	Thumbnail       string
	Status          bool
	AnimationStatus bool
}

func main() {
	fmt.Println("MySql demo")
	db, err := sql.Open("mysql", "root:123456@tcp(127.0.0.1:3306)/tutt")

	if err != nil {
		panic(err.Error())
	}

	defer db.Close()
}
