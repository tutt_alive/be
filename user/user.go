package user

// User with infomation
type User struct {
	ID string
	Displayname string
	Diamon int64
	Avatar string
}

/*
CREATE TABLE users (
	ID bigint,
	Displayname varchar(255),
	Diamond bigint,
	Avatar varchar(255)
);

*/

// AllUser are fake to test
var AllUser []User

